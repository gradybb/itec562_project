<?php include('database.php');?>
<!doctype html>
<html>
<head><title>Films by Category</title></head>
<link rel="stylesheet" type="text/css" href="css/select.css" />

<img src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/MySQL.svg/640px-MySQL.svg.png" alt="logo" style="float:center; max-height:50px;"></br>
<style>
table {
	border:1px solid white;
	width:60%;
	margin-top:20px;
}
td,th {
	border: 2px solid white;
}
td {
	background-color:#DBFEFD;
}
tr{
	font-size:14px;
	height:20px;
	background-color:white;
}
</style>
<div class="nav" id="nav">
<ul>
  <a href="index.html">Home</a>
  <a href="contact_form.php">Contact Form</a>
  <a href="film_query.php">Movies</a>
  <a href="most_rented.php">Most Popular</a>
  <a class="active" href="select_adhoc.php">Select Query</a>
  <a href="update_adhoc.php">Update Query</a>
  <a href="sources_page.html">Resources</a>
  </ul>
  </div>
  
 <body>
 <h1>Select Query</h1>
 
 <form action="select_adhoc.php" method="POST">
<select name="cat">
 <?php
 //
 $sql = "SELECT * FROM category"; // Selects category table to get categories
 
 $result = $conn->query($sql);
 while ($row = $result->fetch_assoc())
 {
	 $id=$row['category_id'];
	 $cat=$row['name'];
	 $options.="<option value=\"$id\">".$cat; //Uses the category ID to populate drop down list with category names
 }
	 ?>
	 <option>
	 <?php echo $options; ?>
	 </option>
</select>
<input type="submit" name="submit" value="submit">
</form>
<div class="cat">
<?php 
// Query that selects the result columns that will go in the table 
$sql = "SELECT film.title AS title, film.rating AS rating,category.category_id AS catid
FROM film
LEFT JOIN film_category ON film_category.film_id = film.film_id
LEFT JOIN category ON film_category.category_id = category.category_id
WHERE category.name=\"$cat\"";

$result = $conn->query($sql);

if (isset($_POST['submit'])) //When form is submitted, results will display in a table
	{
	echo "<table align=center>";
	echo "<tr><th>Title</th><th>Rating</th></tr>";
		while ($row = $result->fetch_assoc()) 
		{
		//This is where the rows are selected
		echo "<tr><td>".$row['title']."</td><td>".$row['rating']."</td></tr>";
	}
}
?>
</table>
</div>
 </body>
 </html>