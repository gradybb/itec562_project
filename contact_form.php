<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<head><title>Contact Information</title></head>

<link rel="stylesheet" href="css/form.css">
<img src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/MySQL.svg/640px-MySQL.svg.png" alt="logo" style="float:center; max-height:50px;"></br>

<div class="nav" id="nav">
<ul>
  <a href="index.html">Home</a>
  <a class="active" href="contact_form.php">Contact Form</a>
  <a href="film_query.php">Movies</a></li>
  <a href="most_rented.php">Most Popular</a>
  <a href="select_adhoc.php">Select</a>
  <a href="update_adhoc.php">Update</a>
  <a href="sources_page.html">Resources</a>
  </ul>
  </div>
<body>

<?php
// Defines variables
$fname="";
$lname="";
$email="";
$phone="";
$address="";
$nameError ="";
$lnameError="";
$emailError ="";
$phoneError ="";
$errflag = false;

// checks that first name field is not null
if ($_SERVER["REQUEST_METHOD"]=="POST") 
{
	$address = $_POST['address'];
	
	if (empty($_POST['fname']))
	{
	$nameError = "First name is required."; 
	$errflag=true; 
	} else 
	{	
	$fname = $_POST['fname']; 
	$errflag=false;
	}
// checks that last name field is not null
	if (empty($_POST['lname']))
	{
	$lnameError = "Last name is required";
	$errflag=true;
	} else	
	{
	$lname = $_POST['lname'];
	$errflag=false;
	}
// checks that email field is not null
	if (empty($_POST['email']))
	{
	$emailError = "Email is required";
	$errflag = true;
	} else 
	{
	$email = $_POST['email'];
	$errflag=false;
	} 
// checks that phone field is not null
	if (empty($_POST['phone'])) 
	{ 
	$phoneError = "Phone number is required.";
	$errflag = true;
	} 
	else 
	{
	$phone = $_POST['phone'];
	$errflag=false;
	}
}
?>

<div class="maindiv">
<form method="post" action="<?php
echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
<center>

<div class="title">
<div class="form_div">
<h1>Contact Information</h1>
<p>* Required Field.</p></div>

<label>First Name:</label><span class="<?php echo $errflag;?>" type="hidden">*</span>
<input type="text" name="fname" value="<?php echo $fname;?>">
<br><br>
<label>Last Name:</label><span class="<?php echo $errflag;?>" type="hidden">*</span>
<input type="text" name="lname" value="<?php echo $lname;?>">
<br><br>
<label>E-mail: </label><span class="<?php echo $errflag;?>" type="hidden">*</span>
<input type="text" name="email" value="<?php echo $email;?>">
<br><br>
<label>Address: </label><span></span>
<input type="input" name="address" value="<?php echo $address;?>">
<br><br>
<label>Phone: </label><span class="<?php echo $errflag;?>" type="hidden">*</span>
<input type="text" name="phone" value="<?php echo $phone;?>">
<br><br>
<input type="submit" name="submit" value="submit" class="submit">
</form></center>
</div>
</div>
</div>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	// shows error message if field isn't valid
	if ($errflag)
		{
		echo "<h2>Please correct errors</h2>";
		echo "<ul>";
		if ($nameError)
			{
			echo "<li>$nameError</li>";
			}
		if ($lnameError){
			echo "<li>$lnameError</li>";
		}
		if ($emailError)
			{
			echo "<li>$emailError</li>";
			}
		if ($phoneError)
			{
			echo "<li>$phoneError</li>";
			}
		echo "</ul>";
		} else // Shows user input if no errors exist
		{
		echo "<h3>Valid Input:</h3>";
		echo "First Name:" . $fname;
		echo "<br />";
		echo "Last Name: " . $lname;
		echo "<br />";
		echo "Email: " . $email;
		echo "<br />";
		echo "Address: " . $address;
		echo "<br />";
		echo "Phone: " . $phone;
		}
}
?>
</body>
</html>