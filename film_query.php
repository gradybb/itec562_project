<?php include('database.php');?>
<!doctype html>
<html>
<head><title>Film List</title></head>
<link rel="stylesheet" href="css/film.css">

<img src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/MySQL.svg/640px-MySQL.svg.png" alt="logo" style="float:center; max-height:50px;"></br>

<div class="nav" id="nav">
<ul>
  <a href="index.html">Home</a>
  <a href="contact_form.php">Contact Form</a>
  <a class="active" href="film_query.php">Movies</a>
  <a href="most_rented.php">Most Popular</a>
  <a href="select_adhoc.php">Select Query</a>
  <a href="update_adhoc.php">Update Query</a>
  <a href="sources_page.html">Resources</a>
  </ul>
  </div>
  
 <body>
 <h1>All Movies</h1>
 <div class="movie">
<?php

$sql = "SELECT title, description, length FROM film";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	echo "<table align=center>";
	echo "<tr><th>Title</th><th>Description</th><th>Length(Minutes)</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>".$row["title"]."</td><td>".$row["description"]."</td><td>".$row["length"]."</td></tr>";
    }
} else {
    echo "0 results";
}
$conn->close();
?> 
</table>
</div>
</body>
</html>