 <?php include('database.php');?>
<!doctype html>
<html>
<link rel="stylesheet" href="css/popular.css">
<head><title>Most Rented</title></head>

<img src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/MySQL.svg/640px-MySQL.svg.png" alt="logo" style="float:center; max-height:50px;"></br>

<div class="nav" id="nav">
<ul>
  <a href="index.html">Home</a>
  <a href="contact_form.php">Contact Form</a>
  <a href="film_query.php">Movies</a>
  <a class="active" href="most_rented.php">Most Popular</a>
  <a href="select_adhoc.php">Select Query</a>
  <a href="update_adhoc.php">Update Query</a>
  <a href="sources_page.html">Resources</a>
  </ul>
  </div>
  
 <body>
 <h1>Top 20 Movies (In Stock)</h1>
 <div class="actor">
 
<?php
 
$sql = "SELECT f.title AS title, 
f.rental_rate AS rate,
f.rating AS rating, 
count(r.rental_id) AS timesrented, 
(SELECT count(*) from inventory WHERE film_id=f.film_id) AS invcount
    FROM film f
    INNER JOIN inventory i
       ON f.film_id = i.film_id
    INNER JOIN rental r
        ON r.inventory_id = i.inventory_id
    GROUP BY f.title
    ORDER BY timesrented DESC
	LIMIT 20";

$result = $conn->query($sql);
if ($result->num_rows > 0) 
	{
	echo "<table align=center>";
	echo "<tr><th id=title>Movie Title</th><th>Rating</th><th>Rental Rate</th><th>Left In Stock</th></tr>";
		while ($row=$result->fetch_assoc()) {
	echo "<tr><td>".$row['title']."</td><td>".$row['rating']."</td><td>".$row['rate']."</td><td>".$row['invcount']."</td></tr>";
	}
}
$conn->close();
?> 
</table>
 </div>
 </body>
 </html>